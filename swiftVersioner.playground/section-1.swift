// Playground - noun: a place where people can play

/*: This script is a post-build script to update the application version
in its preferences plist file from its git build version



*/

/******************************** helper functions **********************************/
import Cocoa

func shell(launchPath: String, arguments: [AnyObject]) -> String
{
    let task = NSTask()
    task.launchPath = launchPath
    task.arguments = arguments

    let pipe = NSPipe()
    task.standardOutput = pipe
    task.launch()

    let data = pipe.fileHandleForReading.readDataToEndOfFile()
    let output: String = NSString(data: data, encoding: NSUTF8StringEncoding)! as String

    return output
}

func bash(bashCommand: String) -> String
{
    let output = shell("/bin/bash", ["-c", bashCommand])
    return output
}

extension String {
    /// Removes a single trailing newline if the string has one.
    func chomp() -> String {
        if self.hasSuffix("\n") {
            return self[self.startIndex..<self.endIndex.predecessor()]
        } else {
            return self
        }
    }
}

func changeCurrentDirectory(newDir: String) -> Bool
{
    let filemgr = NSFileManager.defaultManager()
    return filemgr.changeCurrentDirectoryPath(newDir)
}

/******************************** script **********************************/

var hi = "hello"
var hiWorld = "\(hi) world"


var RunOnlyOnRelease = false

// var env = ENV["XCODE_VERSION_ACTUAL"]
var PATH = NSProcessInfo.processInfo().environment["PATH"] as? String

print(PATH.dynamicType)

/******************************** getting environment for build context **********************************/

var XCODE_VERSION_ACTUAL = NSProcessInfo.processInfo().environment["XCODE_VERSION_ACTUAL"] as? String
var BUILD_STYLE = NSProcessInfo.processInfo().environment["BUILD_STYLE"] as? String
var BUILT_PRODUCTS_DIR = NSProcessInfo.processInfo().environment["BUILT_PRODUCTS_DIR"] as? String
var INFOPLIST_PATH = NSProcessInfo.processInfo().environment["INFOPLIST_PATH"] as? String
var SOURCE_ROOT = NSProcessInfo.processInfo().environment["SOURCE_ROOT"] as? String
var TARGET_BUILD_DIR = NSProcessInfo.processInfo().environment["TARGET_BUILD_DIR"] as? String
var EXECUTABLE_FOLDER_PATH = NSProcessInfo.processInfo().environment["EXECUTABLE_FOLDER_PATH"] as? String


XCODE_VERSION_ACTUAL = "6.3beta"
BUILD_STYLE = "Release"
BUILT_PRODUCTS_DIR = "/Development/ESLForm/Build/"
INFOPLIST_PATH = "ESLForm.app/Info.plist"
SOURCE_ROOT = "/Development/eslform"
TARGET_BUILD_DIR = "/Users/jdmuys/Library/Developer/Xcode/DerivedData/ESLForm-fheozbucfzxlpzclyvvfyyctuxqc/Build/Products/Debug-iphonesimulator"
EXECUTABLE_FOLDER_PATH = "ESLForm.app"

if XCODE_VERSION_ACTUAL == nil {
    NSException.raise("Exception", format:"Must be run from Xcode", arguments:getVaList([]))
}

if RunOnlyOnRelease && BUILD_STYLE != "Release" {
    NSException.raise("Exception", format:"Not a Release build", arguments:getVaList([]))
}

print(INFOPLIST_PATH.dynamicType)

if let sBUILT_PRODUCTS_DIR = BUILT_PRODUCTS_DIR, sINFOPLIST_PATH = INFOPLIST_PATH {
    println("OK environment")
} else {
    println("Wrong environment")
}

/******************************** asking git about version info **********************************/

// var git = shell("sh /etc/profile; which git", [])
// var git = shell("git", ["--version"])

var out = shell("/usr/bin/env", ["pwd"])
out = bash("pwd")
changeCurrentDirectory("/Users/jdmuys/")
out = bash("pwd")


out = shell("/bin/bash", ["-c", "cd /Development; pwd"])
out = shell("/usr/bin/env", ["pwd"])

var git = bash("/etc/profile; which git").chomp()

var bashCommand = "cd '\(SOURCE_ROOT!)'; \(git) describe --tags --always --long"
var tag_age_sha = bash("cd '\(SOURCE_ROOT!)'; \(git) describe --tags --always --long")
tag_age_sha = "3.2B1-0-g2354a8c"
let components = tag_age_sha.componentsSeparatedByString("-")
let tag  = components[0]
let age = components[1]
let ageAsInt = age.toInt()
let sha = components[2]

var branch = bash("cd '\(SOURCE_ROOT!)'; \(git) name-rev HEAD --name-only --always").chomp()
branch = "master"

var version = tag.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 0 ? (ageAsInt == 0 ? "\(tag) (\(sha))" : "\(tag).\(age) (\(sha))") : "(\(sha))"
version = "(be0e612)"

var pListFullPath : String = BUILT_PRODUCTS_DIR! + INFOPLIST_PATH!
println(pListFullPath)

var pListDic = NSMutableDictionary(contentsOfFile:pListFullPath)
print(pListDic.dynamicType)

// let oldBundleVersion = pListDic["CFBundleVersion"] // as? String















