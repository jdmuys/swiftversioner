//
//  main.swift
//  swiftool
//
//  Created by Jean-Denis Muys on 20/02/2015.
//  Copyright (c) 2015 Jean-Denis Muys. All rights reserved.
//

import Foundation


/******************************** helper functions **********************************/
func shell(launchPath: String, arguments: [AnyObject]) -> String
{
    let task = NSTask()
    task.launchPath = launchPath
    task.arguments = arguments

    let pipe = NSPipe()
    task.standardOutput = pipe
    task.launch()

    let data = pipe.fileHandleForReading.readDataToEndOfFile()
    let output: String = NSString(data: data, encoding: NSUTF8StringEncoding)! as String

    return output
}

func bash(bashCommand: String) -> String
{
    let output = shell("/bin/bash", ["-c", bashCommand])
    return output
}

extension String {
    /// Removes a single trailing newline if the string has one.
    func chomp() -> String {
        if self.hasSuffix("\n") {
            return self[self.startIndex..<self.endIndex.predecessor()]
        } else {
            return self
        }
    }
}

func changeCurrentDirectory(newDir: String) -> Bool
{
    let filemgr = NSFileManager.defaultManager()
    return filemgr.changeCurrentDirectoryPath(newDir)
}

/******************************** script **********************************/

println("Hello, World!")

let runOnlyOnRelease = false
let standAloneTest = false

/******************************** getting environment for build context **********************************/

var XCODE_VERSION_ACTUAL = NSProcessInfo.processInfo().environment["XCODE_VERSION_ACTUAL"] as? String
var BUILD_STYLE = NSProcessInfo.processInfo().environment["BUILD_STYLE"] as? String
var BUILT_PRODUCTS_DIR = NSProcessInfo.processInfo().environment["BUILT_PRODUCTS_DIR"] as? String
var INFOPLIST_PATH = NSProcessInfo.processInfo().environment["INFOPLIST_PATH"] as? String
var SOURCE_ROOT = NSProcessInfo.processInfo().environment["SOURCE_ROOT"] as? String
var TARGET_BUILD_DIR = NSProcessInfo.processInfo().environment["TARGET_BUILD_DIR"] as? String
var EXECUTABLE_FOLDER_PATH = NSProcessInfo.processInfo().environment["EXECUTABLE_FOLDER_PATH"] as? String

if standAloneTest {
    XCODE_VERSION_ACTUAL = "0631"
    BUILD_STYLE = ""
    BUILT_PRODUCTS_DIR = "/Users/jdmuys/Library/Developer/Xcode/DerivedData/ESLForm-fheozbucfzxlpzclyvvfyyctuxqc/Build/Products/Release-iphoneos"
    INFOPLIST_PATH = "ESLForm.app/Info.plist"
    SOURCE_ROOT = "/Development/eslform"
    TARGET_BUILD_DIR = "/Users/jdmuys/Library/Developer/Xcode/DerivedData/ESLForm-fheozbucfzxlpzclyvvfyyctuxqc/Build/Products/Release-iphoneos"
    EXECUTABLE_FOLDER_PATH = "ESLForm.app"
}

if XCODE_VERSION_ACTUAL == nil {
    NSException.raise("Exception", format:"Must be run from Xcode", arguments:getVaList([]))
}

if runOnlyOnRelease && BUILD_STYLE != "Release" {
    NSException.raise("Exception", format:"Not a Release build", arguments:getVaList([]))
}



/******************************** asking git about version info **********************************/


let git = bash("/etc/profile; which git").chomp()

let bashCommand = "cd '\(SOURCE_ROOT!)'; \(git) describe --tags --always --long"
let tag_age_sha = bash("cd '\(SOURCE_ROOT!)'; \(git) describe --tags --always --long").chomp()

println("tag_age_sha = \(tag_age_sha)")

let components = tag_age_sha.componentsSeparatedByString("-")
let tag  = components.count == 3 ? components[0] : ""
let age = components.count == 3 ? components[1] : "0"
let ageAsInt = age.toInt()
let sha = components.count == 3 ? components[2] : components[0]

let branch = bash("cd '\(SOURCE_ROOT!)'; \(git) name-rev HEAD --name-only --always").chomp()
println("branch = \(branch)")

let version = tag.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 0 ? (ageAsInt == 0 ? "\(tag) (\(sha))" : "\(tag).\(age) (\(sha))") : "(\(sha))"
println("Version = \(version)")


/******************************** changing Plist file **********************************/

let pListFullPath = String.pathWithComponents([BUILT_PRODUCTS_DIR!, INFOPLIST_PATH!])
//var pListFullPath : String = BUILT_PRODUCTS_DIR! + INFOPLIST_PATH!
println("pListFullPath = \(pListFullPath)")

let pListDic = NSMutableDictionary(contentsOfFile:pListFullPath)!
//println(pListDic.dynamicType)

let oldBundleVersion = pListDic["CFBundleVersion"] as? String
println(" oldBundleVersion = \(oldBundleVersion)")

pListDic["CFBundleVersion"] = version
pListDic["CFBundleShortVersionString"] = version
println("Info.plist CFBundleVersion and CFBundleShortVersionString set to '\(version)'")

pListDic["BuildRevision"] = sha
println("Info.plist BuildRevision set to '\(sha)'")

pListDic["BuildTag"] = tag
println("Info.plist BuildTag set to '\(tag)'")

//println("Whole info.plist = \(pListDic)")


pListDic.writeToFile(pListFullPath, atomically:true)


/******************************** changing user defaults **********************************/

let userDefaults = String.pathWithComponents([TARGET_BUILD_DIR!, EXECUTABLE_FOLDER_PATH!, "Settings.bundle", "Root.plist"])
println("userDefaults = \(userDefaults)")

println("changing user defaults at \(userDefaults)")

let defaults = NSMutableDictionary(contentsOfFile:userDefaults)!
let defaultArray : NSArray = defaults["PreferenceSpecifiers"] as! NSArray
let appVersionDefault : NSMutableDictionary = defaultArray[1] as! NSMutableDictionary
appVersionDefault["DefaultValue"] = version


defaults.writeToFile(userDefaults, atomically:true)


println("default app_version set to '\(version)'")


println("That's all folks")

exit(0)




