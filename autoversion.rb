##/usr/bin/env macruby

puts "Automatic versioning..."

# per: http://rubycocoa.sourceforge.net/RequireFramework
framework 'Cocoa'
#include OSX
#OSX.require_framework 'Foundation'

RunOnlyOnRelease = false

raise "Must be run from Xcode" unless ENV['XCODE_VERSION_ACTUAL']
raise "Not a Release build" if ENV["BUILD_STYLE"] != "Release" and RunOnlyOnRelease

plist = File.join ENV['BUILT_PRODUCTS_DIR'], ENV['INFOPLIST_PATH']
raise "Info.plist missing, or path error" unless File.file? plist

userDefaults = File.join ENV['TARGET_BUILD_DIR'], ENV['EXECUTABLE_FOLDER_PATH'], "Settings.bundle", "Root.plist"

git = `sh /etc/profile; which git`.chomp
tag, age, sha = `"#{git}" describe --tags --always --long`.chomp.match(/(?:(.*)-(\d+)-)?([0-9a-g]+)/i)[1..3]
branch = `"#{git}" name-rev HEAD --name-only --always`.chomp

# "v1.0", "v1.0b2 [g3f22c9f]", "[g3f22c9f]"
version = tag ? (age.to_i.zero? ? "#{tag} (#{sha})" : "#{tag}.#{age} (#{sha})") : "(#{sha})"
#version += " [#{branch}]" if branch and branch != 'master'

puts "changing Info.plist at #{plist}"
list = NSMutableDictionary.dictionaryWithContentsOfFile plist
puts "static version=#{list['CFBundleVersion']}, setting to #{version}"
list['CFBundleVersion'] = version
list['CFBundleShortVersionString'] = version
puts "Info.plist CFBundleVersion and CFBundleShortVersionString set to '#{version}'!"
list['BuildRevision'] = sha
puts "Info.plist BuildRevision set to '#{sha}'!"
list['BuildTag'] = tag
puts "Info.plist BuildTag set to '#{tag}'!"
puts "Whole info.plist = #{list}"
list.writeToFile(plist, atomically:true)

puts "changing user defaults at #{userDefaults}"
defaults = NSMutableDictionary.dictionaryWithContentsOfFile userDefaults
defaultArray = defaults['PreferenceSpecifiers']
appVersionDefault = defaultArray[1]
appVersionDefault['DefaultValue'] = version
defaults.writeToFile(userDefaults, atomically:true)

puts "default app_version set to '#{version}'!"


################################
#!/bin/bash

#GIT=/usr/local/git/bin/git
#GITOPTIONS="--pretty=oneline --abbrev-commit"
#CUT=/usr/bin/cut

# the hash value (first 7 chars)
#/usr/libexec/PlistBuddy -c "Set :BuildRevision `$GIT log -1 $GITOPTIONS | $CUT -c1-7`" ${TARGET_BUILD_DIR}/${INFOPLIST_PATH}

# also set the version piece of the user defaults
#/usr/libexec/PlistBuddy -c "Set :PreferenceSpecifiers:1:DefaultValue `$GIT log -1 $GITOPTIONS | $CUT -c1-7`" ${TARGET_BUILD_DIR}/${EXECUTABLE_FOLDER_PATH}/Settings.bundle/Root.plist

#/usr/libexec/PlistBuddy -c "Set :BuildTag $(git tag -l --contains $(git log -1 --pretty=oneline --abbrev-commit | cut -c1-7))" ${TARGET_BUILD_DIR}/${INFOPLIST_PATH}
